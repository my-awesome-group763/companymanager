package com.desafio.application.company.create;

import com.desafio.application.company.find.FindCompanyByTickerCommand;
import com.desafio.application.company.find.FindCompanyByTickerUseCase;
import com.desafio.application.user.create.CreateUserOutput;
import com.desafio.domain.company.Company;
import com.desafio.domain.company.CompanyGateway;
import com.desafio.log.ILog;
import com.desafio.log.Log;

import java.math.BigDecimal;

import static com.desafio.utils.Constants.ActionMessages.CREATE_COMPANY;
import static com.desafio.utils.Constants.CLASS_STR;
import static com.desafio.utils.Constants.ErrorMessages.COMPANY_ALREADY_EXISTS;
import static com.desafio.utils.Constants.ErrorMessages.GATEWAY_INSERT_STR;
import static com.desafio.utils.Constants.SuccessMessages.*;

public class CreateCompanyUseCase {

  private static final ILog log = new Log(CreateCompanyUseCase.class);
  private final CompanyGateway companyGateway;

  private CreateCompanyUseCase(CompanyGateway gateway) {
    this.companyGateway = gateway;
  }

  public static CreateCompanyUseCase create(CompanyGateway gateway) {
    return new CreateCompanyUseCase(gateway);
  }

  public CreateCompanyOutput execute(CreateCompanyCommand command) {
    Company company = this.createCompany(command);

    company.validate();

    if (company.getNotification().hasErrors())
      return CreateCompanyOutput.from(company.getNotification());

    log.info(COMPANY_VALIDATED, company.getName());

    if (!this.hasCompany(company.getTicker().getValue())) {
      String message = COMPANY_ALREADY_EXISTS.replace("{}", company.getTicker().getValue());
      log.info(message);
      return CreateCompanyOutput.from(message, CLASS_STR);
    }

    try {
      CreateCompanyOutput output = CreateCompanyOutput.from(this.companyGateway.create(company));
      log.info(COMPANY_INSERTED_ON_GATEWAY, output.ticker());
      return output;
    } catch (Exception e) {
      return CreateCompanyOutput.from(GATEWAY_INSERT_STR.replace("{}", company.getTicker().getValue()), CLASS_STR);
    }
  }

  private boolean hasCompany(String ticker) {
    return FindCompanyByTickerUseCase.with(this.companyGateway)
            .execute(FindCompanyByTickerCommand.create(ticker)).notificationErrors() != null;
  }

  private Company createCompany(CreateCompanyCommand command) {
    final String name = command.name();
    final String ticker = command.ticker();
    final BigDecimal price = command.price();

    log.info(CREATE_COMPANY, name, ticker, price.toString());

    Company company = Company.create(name, ticker, price);
    log.info(USER_CREATED, company.getId().getValue());

    return company;
  }
}
