package com.desafio.application.company.create;

import com.desafio.domain.company.Company;
import com.desafio.notification.INotification;
import com.desafio.notification.Notification;
import com.desafio.notification.NotificationErrorProps;

import java.math.BigDecimal;

public record CreateCompanyOutput(String id, String name, String ticker, BigDecimal price, Boolean isActive, INotification notificationErrors) {

  public static CreateCompanyOutput from(INotification notification) {
    return new CreateCompanyOutput(null, null, null, null, null, notification);
  }

  public static CreateCompanyOutput from(String name, String ticker, BigDecimal price, Boolean isActive) {
    return new CreateCompanyOutput(null, name, ticker, price, isActive, null);
  }

  public static CreateCompanyOutput from(String id, String name, String ticker, BigDecimal price, Boolean isActive) {
    return new CreateCompanyOutput(id, name, ticker, price, isActive, null);
  }

  public static CreateCompanyOutput from(String message, String context) {
    INotification notification = new Notification();
    notification.append(new NotificationErrorProps(message, context));
    return new CreateCompanyOutput(null, null, null, null, null, notification);
  }

  public static CreateCompanyOutput from(Company company) {
    String id = company.getId().getValue();
    String name = company.getName();
    String ticker = company.getTicker().getValue();
    BigDecimal price = company.getPrice();
    boolean isActive = company.isActive();

    return new CreateCompanyOutput(id, name, ticker, price, isActive,null);
  }
}
