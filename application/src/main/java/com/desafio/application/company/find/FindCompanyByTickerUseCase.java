package com.desafio.application.company.find;

import com.desafio.domain.company.Company;
import com.desafio.domain.company.CompanyGateway;
import com.desafio.log.ILog;
import com.desafio.log.Log;

import java.util.Optional;

import static com.desafio.utils.Constants.CLASS_STR;
import static com.desafio.utils.Constants.ErrorMessages.COMPANY_DOES_NOT_EXISTS;
import static com.desafio.utils.Constants.ErrorMessages.GATEWAY_FIND_STR;

public class FindCompanyByTickerUseCase {

  private static final ILog log = new Log(FindCompanyByTickerUseCase.class);

  private final CompanyGateway companyGateway;

  public FindCompanyByTickerUseCase(CompanyGateway gateway) {
    this.companyGateway = gateway;
  }

  public static FindCompanyByTickerUseCase with(CompanyGateway gateway) {
    return new FindCompanyByTickerUseCase(gateway);
  }

  public FindCompanyByTickerOutput execute(FindCompanyByTickerCommand command) {
    Optional<Company> company = Optional.empty();
    try {
       company = this.companyGateway.findByTicker(command.ticker());
    } catch (Exception e) {
      return FindCompanyByTickerOutput.from(GATEWAY_FIND_STR.replace("{}", command.ticker()));
    }


    if (company.isEmpty()) {
      String message = COMPANY_DOES_NOT_EXISTS.replace("{}", command.ticker());
      log.info(message);
      return FindCompanyByTickerOutput.from(message);
    }

    return FindCompanyByTickerOutput.from(company.get());
  }
}
