package com.desafio.application.company.find;

import com.desafio.domain.company.Company;
import com.desafio.notification.INotification;
import com.desafio.notification.Notification;
import com.desafio.notification.NotificationErrorProps;
import com.desafio.utils.Constants;

import java.math.BigDecimal;
import java.time.Instant;

import static com.desafio.utils.Constants.CLASS_STR;

public record FindCompanyByTickerOutput(
        String id,
        String name,
        String ticker,
        BigDecimal price,
        Boolean isActive,
        Instant createdAt,
        Instant updatedAt,
        Instant deletedAt,
        INotification notificationErrors) {

  public static FindCompanyByTickerOutput from(String message) {
    INotification notification = new Notification();
    notification.append(new NotificationErrorProps(message, CLASS_STR));
    return new FindCompanyByTickerOutput(
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            notification);
  }

  public static FindCompanyByTickerOutput from(Company company) {
    return new FindCompanyByTickerOutput(
            company.getId().getValue(),
            company.getName(),
            company.getTicker().getValue(),
            company.getPrice(),
            company.isActive(),
            company.getCreatedAt(),
            company.getUpdatedAt(),
            company.getDeletedAt(),
            null);
  }

}
