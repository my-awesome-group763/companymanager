package com.desafio.application.company.update;

import com.desafio.application.company.find.FindCompanyByTickerCommand;
import com.desafio.application.company.find.FindCompanyByTickerOutput;
import com.desafio.application.company.find.FindCompanyByTickerUseCase;
import com.desafio.domain.company.Company;
import com.desafio.domain.company.CompanyGateway;
import com.desafio.domain.company.ticker.Ticker;
import com.desafio.notification.INotification;
import com.desafio.notification.Notification;
import com.desafio.utils.Constants;
import jdk.jfr.Category;

import java.util.Optional;

import static com.desafio.utils.Constants.CLASS_STR;
import static com.desafio.utils.Constants.ErrorMessages.GATEWAY_UPDATE_STR;

public class UpdateCompanyUseCase {

  private final CompanyGateway companyGateway;

  public UpdateCompanyUseCase(CompanyGateway gateway) {
    this.companyGateway = gateway;
  }

  public UpdateCompanyOutput execute(UpdateCompanyCommand command) {
    final var name = command.name();
    final var ticker = command.ticker();
    final var price = command.price();
    final var isActive = command.isActive();

    final var findCompanyByTickerOutput = this.findCompany(ticker);

    if (findCompanyByTickerOutput.notificationErrors() != null)
      return UpdateCompanyOutput.from(findCompanyByTickerOutput.notificationErrors());

    Company company = this.createCompanyToUpdate(findCompanyByTickerOutput);
    company.update(name, Ticker.create(ticker), price, isActive).validate();

    if (company.getNotification().hasErrors())
      return UpdateCompanyOutput.from(company.getNotification());

    return this.updateCompany(company);
  }

  private UpdateCompanyOutput updateCompany(Company company) {
    try {
      this.companyGateway.update(company);
    } catch (Exception e) {
      String message = GATEWAY_UPDATE_STR.replace("{}", company.toString());
      return UpdateCompanyOutput.from(message, CLASS_STR);
    }

    return UpdateCompanyOutput.from(company);
  }

  private Company createCompanyToUpdate(FindCompanyByTickerOutput findCompanyByTickerOutput) {
    return Company.create(
            findCompanyByTickerOutput.name(),
            Ticker.create(findCompanyByTickerOutput.ticker()),
            findCompanyByTickerOutput.price()
    );
  }

  private FindCompanyByTickerOutput findCompany(String ticker) {
    return FindCompanyByTickerUseCase.with(companyGateway).execute(FindCompanyByTickerCommand.create(ticker));
  }

}
