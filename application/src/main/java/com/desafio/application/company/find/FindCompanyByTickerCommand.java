package com.desafio.application.company.find;

public record FindCompanyByTickerCommand(String ticker) {
  public static FindCompanyByTickerCommand create(String ticker) {
    return new FindCompanyByTickerCommand(ticker);
  }
}
