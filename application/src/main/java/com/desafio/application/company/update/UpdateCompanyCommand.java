package com.desafio.application.company.update;

import java.math.BigDecimal;

public record UpdateCompanyCommand(String name, String ticker, BigDecimal price, String user_cpf, boolean isActive) {

  public static UpdateCompanyCommand with(String name, String ticker, BigDecimal price, String user_cpf, boolean isActive) {
    return new UpdateCompanyCommand(name, ticker, price, user_cpf, isActive);
  }

}
