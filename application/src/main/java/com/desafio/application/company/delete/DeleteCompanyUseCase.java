package com.desafio.application.company.delete;

import com.desafio.application.company.find.FindCompanyByTickerUseCase;
import com.desafio.domain.company.CompanyGateway;
import com.desafio.log.ILog;
import com.desafio.log.Log;
import com.desafio.utils.Constants;

public class DeleteCompanyUseCase {

  private static final ILog log = new Log(FindCompanyByTickerUseCase.class);

  private final CompanyGateway companyGateway;

  public DeleteCompanyUseCase(CompanyGateway gateway) {
    this.companyGateway = gateway;
  }

  public static DeleteCompanyUseCase with(CompanyGateway gateway) {
    return new DeleteCompanyUseCase(gateway);
  }

  public DeleteCompanyOutput execute(DeleteCompanyCommand command) {
    try {
      this.companyGateway.delete(command.ticker());
      return null;
    } catch (Exception e) {
      return DeleteCompanyOutput.from(Constants.ErrorMessages.GATEWAY_DELETE_STR);
    }
  }
}
