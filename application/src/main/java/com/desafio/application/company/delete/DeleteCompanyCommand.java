package com.desafio.application.company.delete;

import com.desafio.application.company.create.CreateCompanyCommand;

import java.math.BigDecimal;

public record DeleteCompanyCommand(String ticker) {

  public static DeleteCompanyCommand with(String ticker) {
    return new DeleteCompanyCommand(ticker);
  }

}
