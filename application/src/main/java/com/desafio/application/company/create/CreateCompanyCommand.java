package com.desafio.application.company.create;

import java.math.BigDecimal;

public record CreateCompanyCommand(String name, String ticker, BigDecimal price, boolean isActive) {

  public static CreateCompanyCommand with(String name, String ticker, BigDecimal price, boolean isActive) {
    return new CreateCompanyCommand(name, ticker, price, isActive);
  }

}
