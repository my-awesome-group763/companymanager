package com.desafio.application.company.update;

import com.desafio.application.company.create.CreateCompanyOutput;
import com.desafio.domain.company.Company;
import com.desafio.notification.INotification;
import com.desafio.notification.Notification;

import java.math.BigDecimal;

import static com.desafio.utils.Constants.CLASS_STR;
import static com.desafio.utils.Constants.ErrorMessages.GATEWAY_UPDATE_STR;

public record UpdateCompanyOutput(String name, String ticker, BigDecimal price, Boolean isActive, INotification notificationErrors) {

  public static UpdateCompanyOutput from(INotification notification) {
    return new UpdateCompanyOutput(null, null, null, null, notification);
  }

  public static UpdateCompanyOutput from(String message, String context) {
    INotification notification = new Notification();
    notification.append(message, CLASS_STR);
    return new UpdateCompanyOutput(null, null, null, null, notification);
  }

  public static UpdateCompanyOutput from(Company company) {
    String name = company.getName();
    String ticker = company.getTicker().getValue();
    BigDecimal price = company.getPrice();
    boolean isActive = company.isActive();

    return new UpdateCompanyOutput(name, ticker, price, isActive,null);
  }
}
