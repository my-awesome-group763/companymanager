package com.desafio.application.company.list;

import com.desafio.domain.company.CompanyGateway;

public class ListCompaniesUseCase {

  private final CompanyGateway companyGateway;

  public ListCompaniesUseCase(CompanyGateway gateway) {
    this.companyGateway = gateway;
  }
}
