package com.desafio.application.company.delete;

import com.desafio.notification.INotification;
import com.desafio.notification.Notification;

import static com.desafio.utils.Constants.CLASS_STR;

public record DeleteCompanyOutput(INotification notification) {

  public static DeleteCompanyOutput from(String message) {
    INotification notification = new Notification();
    notification.append(message, CLASS_STR);
    return new DeleteCompanyOutput(notification);
  }

}
