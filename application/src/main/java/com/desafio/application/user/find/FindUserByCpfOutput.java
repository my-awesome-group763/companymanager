package com.desafio.application.user.find;

import com.desafio.domain.user.User;
import com.desafio.notification.INotification;
import com.desafio.notification.Notification;
import com.desafio.notification.NotificationErrorProps;

import static com.desafio.utils.Constants.USER_STR;

public record FindUserByCpfOutput(String name, String cpf, INotification notificationErrors) {

  public static FindUserByCpfOutput from(User user) {
    return new FindUserByCpfOutput(user.getName(), user.getCpf().getValue(), null);
  }

  public static FindUserByCpfOutput from(INotification notification) {
    return new FindUserByCpfOutput(null, null, notification);
  }

  public FindUserByCpfOutput(String name, String cpf) {
    this(name, cpf, null);
  }

  public static FindUserByCpfOutput from(String message) {
    Notification notification = new Notification();
    NotificationErrorProps userAlreadyExists = new NotificationErrorProps(message, USER_STR);
    notification.append(userAlreadyExists);
    return new FindUserByCpfOutput(null, null, notification);
  }
}