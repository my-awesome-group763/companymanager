package com.desafio.application.user.create;

import com.desafio.domain.user.User;
import com.desafio.notification.INotification;
import com.desafio.notification.Notification;

import static com.desafio.utils.Constants.CLASS_STR;
import static com.desafio.utils.Constants.USER_STR;

public record CreateUserOutput(String name, String cpf, INotification notificationErrors) {

  public static CreateUserOutput from(User user) {
    return new CreateUserOutput(user.getName(), user.getCpf().getValue(), null);
  }

  public static CreateUserOutput from(INotification notification) {
    return new CreateUserOutput(null, null, notification);
  }

  public static CreateUserOutput from(String message) {
    INotification notification = new Notification();
    notification.append(message, USER_STR);
    return new CreateUserOutput(null, null, notification);
  }

  public CreateUserOutput(String name, String cpf) {
    this(name, cpf, null);
  }
}
