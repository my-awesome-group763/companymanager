package com.desafio.application.user.find;

public record FindUserByCpfCommand (String cpf) {

  public static FindUserByCpfCommand with(String cpf) {
    return new FindUserByCpfCommand(cpf);
  }

}
