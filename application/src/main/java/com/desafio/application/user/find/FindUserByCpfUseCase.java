package com.desafio.application.user.find;

import com.desafio.domain.user.User;
import com.desafio.domain.user.UserGateway;
import com.desafio.domain.user.cpf.CpfValidator;
import com.desafio.log.ILog;
import com.desafio.log.Log;

import java.util.Optional;

import static com.desafio.utils.Constants.ActionMessages.FIND_BY_CPF;
import static com.desafio.utils.Constants.ErrorMessages.*;

public class FindUserByCpfUseCase {

  private static final ILog log = new Log(FindUserByCpfUseCase.class);

  private UserGateway userGateway;

  private FindUserByCpfUseCase(UserGateway gateway) {
    this.userGateway = gateway;
  }

  public static FindUserByCpfUseCase with(UserGateway gateway) {
    return new FindUserByCpfUseCase(gateway);
  }

  public FindUserByCpfOutput execute(FindUserByCpfCommand command) {
    log.info(FIND_BY_CPF, command.cpf());

    if (!CpfValidator.isCPF(command.cpf()))
      return FindUserByCpfOutput.from(INVALID_CPF.replace("{}", command.cpf()));

    try {
      Optional<User> actualUser = this.userGateway.findByCpf(command.cpf());

      if(actualUser.isEmpty())
        return FindUserByCpfOutput.from(USER_DOES_NOT_EXISTS.replace("{}", command.cpf()));

      return FindUserByCpfOutput.from(actualUser.get());

    } catch (Exception e) {
      return FindUserByCpfOutput.from(GATEWAY_FIND_STR.replace("{}", command.cpf()));
    }

  }
}
