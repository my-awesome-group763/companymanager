package com.desafio.application.user.create;

import com.desafio.application.user.find.FindUserByCpfCommand;
import com.desafio.application.user.find.FindUserByCpfUseCase;
import com.desafio.domain.user.User;
import com.desafio.domain.user.UserGateway;
import com.desafio.log.ILog;
import com.desafio.log.Log;

import java.util.Objects;

import static com.desafio.utils.Constants.ActionMessages.CREATE_USER;
import static com.desafio.utils.Constants.ErrorMessages.*;
import static com.desafio.utils.Constants.SuccessMessages.*;

public class CreateUserUseCase {

  private static final ILog log = new Log(CreateUserUseCase.class);

  private final UserGateway userGateway;

  private CreateUserUseCase(UserGateway userGateway) {
    this.userGateway = Objects.requireNonNull(userGateway);
  }

  public static CreateUserUseCase create(UserGateway userGateway) {
    return new CreateUserUseCase(userGateway);
  }

  public CreateUserOutput execute(CreateUserCommand createUserCommand) {
    User user = this.createUser(createUserCommand);

    user.validate();

    if (user.getNotification().hasErrors())
      return CreateUserOutput.from(user.getNotification());

    log.info(USER_VALIDATED, user.getName());

    if (!this.hasUser(user.getCpf().getValue())) {
      String message = USER_ALREADY_EXISTS.replace("{}", user.getCpf().getValue());
      log.info(message);
      return CreateUserOutput.from(message);
    }

    return this.insertUser(user);
  }

  private CreateUserOutput insertUser(User user) {
    try {
      CreateUserOutput output = CreateUserOutput.from(this.userGateway.create(user));
      log.info(USER_INSERTED_ON_GATEWAY.replace("{}", user.toString()), output.cpf());
      return output;
    } catch (Exception e) {
      return  CreateUserOutput.from(GATEWAY_INSERT_STR.replace("{}", user.getCpf().getValue()));
    }
  }

  private boolean hasUser(String cpf) {
    return FindUserByCpfUseCase.with(this.userGateway)
            .execute(new FindUserByCpfCommand(cpf)).notificationErrors() != null;
  }

  private User createUser(CreateUserCommand createUserCommand) {
    final String name = createUserCommand.name();
    final String cpf = createUserCommand.cpf();

    log.info(CREATE_USER, name, cpf);

    User user = User.create(name, cpf);
    log.info(USER_CREATED, user.getId().getValue());

    return user;
  }

}
