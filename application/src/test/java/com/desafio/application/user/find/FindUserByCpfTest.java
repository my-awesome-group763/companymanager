package com.desafio.application.user.find;

import com.desafio.domain.user.User;
import com.desafio.domain.user.UserGateway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.desafio.utils.Constants.*;
import static com.desafio.utils.Constants.ErrorMessages.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FindUserByCpfTest {

  @Mock
  private UserGateway userGateway;

  @InjectMocks
  private FindUserByCpfUseCase useCase;

  @Test
  void givenValidCPF_whenCallsFindByCPF_shouldReturnUser() {
    final Optional<User> expected_user = Optional.of(User.create(NAME_SAMPLE, CPF_SAMPLE));
    FindUserByCpfCommand command = FindUserByCpfCommand.with(CPF_SAMPLE);

    when(userGateway.findByCpf(anyString())).thenReturn(expected_user);

    FindUserByCpfOutput output = useCase.execute(command);

    Assertions.assertNotNull(output);
    Assertions.assertEquals(NAME_SAMPLE, output.name());
  }

  @Test
  void givenValidCPF_whenGatewayThrowException_shouldThrowException() {
    FindUserByCpfCommand command = FindUserByCpfCommand.with(CPF_SAMPLE);
    String expected_error_message = "[USER]: COULD NOT FIND OBJECT {}".replace("{}", command.cpf());
    when(userGateway.findByCpf(command.cpf())).thenThrow(new IllegalStateException(GATEWAY_ERROR_STR));

    FindUserByCpfOutput output = useCase.execute(command);

    Assertions.assertEquals(expected_error_message, output.notificationErrors().messages(""));

    verify(userGateway, times(1)).findByCpf(command.cpf());
  }

//  @Test
//  void givenInvalidCPF_whenCallsFindByCPF_shouldReturnNothing() {
//    FindUserByCpfCommand command = FindUserByCpfCommand.with("123");
//    String expectedMessage = "["+USER_STR+"]: "+INVALID_CPF.replace("{}", "123");
//
//    when(userGateway.findByCpf(anyString())).thenReturn(Optional.empty());
//
//    FindUserByCpfOutput output = useCase.execute(command);
//
//    Assertions.assertEquals(expectedMessage, output.notificationErrors().messages(""));
//  }

}
