package com.desafio.application.user.create;

import com.desafio.domain.user.User;
import com.desafio.domain.user.UserGateway;
import com.desafio.domain.user.cpf.Cpf;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Objects;
import java.util.Optional;

import static com.desafio.utils.Constants.*;
import static com.desafio.utils.Constants.ErrorMessages.GATEWAY_ERROR_STR;
import static com.desafio.utils.Constants.ErrorMessages.USER_ALREADY_EXISTS;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CreateUserUseCaseTest {

  @Mock
  private UserGateway userGateway;

  @InjectMocks
  private CreateUserUseCase useCase;

  @Test
  void givenValidCommand_whenCallsCreateCategory_shouldReturnCategoryId() {
    final var command = CreateUserCommand.with(NAME_SAMPLE, CPF_SAMPLE, true);

    when(userGateway.create(any())).thenAnswer(returnsFirstArg());

    final CreateUserOutput output = useCase.execute(command);

    Assertions.assertNotNull(output);
    Assertions.assertNotNull(output.cpf());

    verify(userGateway, times(1)).create(Mockito.argThat(user ->
            Objects.equals(NAME_SAMPLE, user.getName()) && Objects.equals(Cpf.create(CPF_SAMPLE), user.getCpf())
    ));
  }

  @Test
  void givenValidCommandAndExistentUser_whenCallsCreateCategory_shouldReturnCategoryId() {
    final var command = CreateUserCommand.with(NAME_SAMPLE, CPF_SAMPLE, true);
    final var expectedMessage = "["+USER_STR+"]: "+USER_ALREADY_EXISTS.replace("{}", CPF_SAMPLE);
    when(userGateway.findByCpf(any())).thenReturn(
            Optional.of(User.create(NAME_SAMPLE, CPF_SAMPLE))
    );

    final var createUser = useCase.execute(command);

    Assertions.assertNotNull(createUser.notificationErrors());
    Assertions.assertEquals(createUser.notificationErrors().messages(""), expectedMessage);

    verify(userGateway, times(1)).findByCpf(any());
  }

  @Test
  void givenInvalidBlankName_whenCallsCreateCategory_shouldThrowAnError() {
    final String expected_error_message = "[USER]: 'NAME' SHOULD NOT BE BLANK.[USER]: 'NAME' MUST BE BETWEEN 3 AND 30 CHARACTERS.";

    final var command = CreateUserCommand.with("", CPF_SAMPLE, true);
    final var output = useCase.execute(command);

    Assertions.assertEquals(expected_error_message, output.notificationErrors().messages(""));
    verify(userGateway, times(0)).create(any());
  }

  @Test
  void givenValidCommand_whenGatewayThrowsAnError_shouldThrowAnError() {
    final var command = CreateUserCommand.with(NAME_SAMPLE, CPF_SAMPLE, true);
    String expected_error_message = "[USER]: COULD NOT INSERT OBJECT {}".replace("{}", command.cpf());
    when(userGateway.create(any())).thenThrow(new RuntimeException(GATEWAY_ERROR_STR));

    CreateUserOutput output = useCase.execute(command);

    Assertions.assertEquals(expected_error_message, output.notificationErrors().messages(""));

    verify(userGateway, times(1)).create(any());
  }
}
