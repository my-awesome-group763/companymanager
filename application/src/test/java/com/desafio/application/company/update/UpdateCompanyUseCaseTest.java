package com.desafio.application.company.update;

import com.desafio.application.UseCaseTest;
import com.desafio.application.company.create.CreateCompanyCommand;
import com.desafio.application.company.find.FindCompanyByTickerCommand;
import com.desafio.domain.company.Company;
import com.desafio.domain.company.CompanyGateway;
import com.desafio.utils.Constants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.desafio.utils.Constants.*;
import static com.desafio.utils.Constants.ErrorMessages.*;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class UpdateCompanyUseCaseTest  extends UseCaseTest {

  @InjectMocks
  private UpdateCompanyUseCase useCase;

  @Mock
  private CompanyGateway companyGateway;

  @Override
  protected List<Object> getMocks() {
    return List.of(companyGateway);
  }

  @Test
  public void givenAValidCommand_whenCallsUpdateCompany_shouldReturnCompany() {
    final var company =
            Company.create(NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE);

    final var updateCommand = UpdateCompanyCommand.with(NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, null, true);
    final var findCommand = FindCompanyByTickerCommand.create(TICKER_SAMPLE);
    when(companyGateway.findByTicker(eq(findCommand.ticker())))
            .thenReturn(Optional.of(company));

    when(companyGateway.update(any()))
            .thenAnswer(returnsFirstArg());

    final var output = useCase.execute(updateCommand);

    Assertions.assertNotNull(output);
    Assertions.assertNotNull(output.ticker());

    verify(companyGateway, times(1)).findByTicker(eq(findCommand.ticker()));

    verify(companyGateway, times(1)).update(argThat(
            aUpdatedCompany ->
                    Objects.equals(NAME_SAMPLE, aUpdatedCompany.getName())
                            && Objects.equals(TICKER_SAMPLE, aUpdatedCompany.getTicker().getValue())
                            && Objects.equals(PRICE_SAMPLE, aUpdatedCompany.getPrice())
                            && Objects.nonNull(aUpdatedCompany.getCreatedAt())
                            && Objects.nonNull(aUpdatedCompany.getUpdatedAt())
                            && company.getUpdatedAt().isBefore(aUpdatedCompany.getUpdatedAt())
                            && Objects.isNull(aUpdatedCompany.getDeletedAt())
    ));
  }

  @Test
  public void givenAInvalidName_whenCallsUpdateCompany_thenShouldReturnListOfErrors() {
    final var company =
            Company.create("", TICKER_SAMPLE, PRICE_SAMPLE);
    final var expectedErrorCount = 2;

    final var findCommand = FindCompanyByTickerCommand.create(TICKER_SAMPLE);
    final var updateCommand = UpdateCompanyCommand.with("", TICKER_SAMPLE, PRICE_SAMPLE, null, true);

    when(companyGateway.findByTicker(eq(findCommand.ticker())))
            .thenReturn(Optional.of(company));

    final var output = useCase.execute(updateCommand);

    Assertions.assertNotNull(output.notificationErrors());
    Assertions.assertEquals(expectedErrorCount, output.notificationErrors().getErrors().size());

    Mockito.verify(companyGateway, times(0)).update(any());
  }

  @Test
  public void givenAValidInactivateCommand_whenCallsUpdateCompany_shouldReturnInactiveCompany() {
    final var company =
            Company.create(NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE);
    final var expectedIsActive = false;
    final var expectedName = "Banco Inter";

    final var findCommand = FindCompanyByTickerCommand.create(TICKER_SAMPLE);
    final var updateCommand = UpdateCompanyCommand.with(expectedName, TICKER_SAMPLE, PRICE_SAMPLE, null, expectedIsActive);

    when(companyGateway.findByTicker(eq(findCommand.ticker())))
            .thenReturn(Optional.of(company));

    when(companyGateway.update(any()))
            .thenAnswer(returnsFirstArg());

    Assertions.assertTrue(company.isActive());
    Assertions.assertNull(company.getDeletedAt());

    final var actualOutput = useCase.execute(updateCommand);

    Assertions.assertNull(actualOutput.notificationErrors());
    Assertions.assertNotNull(actualOutput.ticker());
    Assertions.assertFalse(actualOutput.isActive());
    Assertions.assertEquals(expectedName, actualOutput.name());

    Mockito.verify(companyGateway, times(1)).findByTicker(eq(findCommand.ticker()));

    Mockito.verify(companyGateway, times(1)).update(any());
  }

  @Test
  public void givenAValidCommand_whenGatewayThrowsRandomException_shouldReturnAException() {
    final var company =
            Company.create("", TICKER_SAMPLE, PRICE_SAMPLE);

    final var expectedName = "Banco Inter";

    final var findCommand = FindCompanyByTickerCommand.create(TICKER_SAMPLE);
    final var updateCommand = UpdateCompanyCommand.with(expectedName, TICKER_SAMPLE, PRICE_SAMPLE, null, true);

    when(companyGateway.findByTicker(eq(findCommand.ticker())))
            .thenReturn(Optional.of(company));

    when(companyGateway.update(any()))
            .thenThrow(new IllegalStateException(GATEWAY_UPDATE_STR));

    final var notification = useCase.execute(updateCommand);

    Assertions.assertEquals(1, notification.notificationErrors().getErrors().size());

    Mockito.verify(companyGateway, times(1)).findByTicker(any());
    Mockito.verify(companyGateway, times(1)).update(any());
  }

  @Test
  public void givenACommandWithInvalidTicker_whenCallsUpdateCompany_shouldReturnNotFoundException() {
    final var company =
            Company.create(NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE);

    final var expectedName = "Banco Inter";
    final var findCommand = FindCompanyByTickerCommand.create(TICKER_SAMPLE);
    final var updateCommand = UpdateCompanyCommand.with(expectedName, TICKER_SAMPLE, PRICE_SAMPLE, null, true);

    when(companyGateway.findByTicker(eq(findCommand.ticker())))
            .thenThrow(new IllegalStateException(GATEWAY_FIND_STR));

    useCase.execute(updateCommand);

    Mockito.verify(companyGateway, times(1)).findByTicker(any());
    Mockito.verify(companyGateway, times(0)).update(any());
  }
}