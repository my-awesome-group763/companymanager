package com.desafio.application.company.delete;

import com.desafio.application.UseCaseTest;
import com.desafio.domain.company.Company;
import com.desafio.domain.company.CompanyGateway;
import com.desafio.domain.company.CompanyId;
import com.desafio.utils.Constants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.List;

import static com.desafio.utils.Constants.*;
import static com.desafio.utils.Constants.ErrorMessages.GATEWAY_DELETE_STR;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class DeleteCompanyUseCaseTest extends UseCaseTest {

  @InjectMocks
  private DeleteCompanyUseCase useCase;

  @Mock
  private CompanyGateway companyGateway;

  @Override
  protected List<Object> getMocks() {
    return null;
  }

  @Test
  public void givenAValidId_whenCallsDeleteCompany_shouldBeOK() {
    final var company = Company.create(NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE);
    final var command = DeleteCompanyCommand.with(company.getTicker().getValue());

    doNothing()
            .when(companyGateway).delete(eq(command.ticker()));

    final var output = useCase.execute(command);

    Assertions.assertNull(output);

    Mockito.verify(companyGateway, times(1)).delete(eq(command.ticker()));
  }

  @Test
  public void givenAInvalidTicker_whenCallsDeleteCompany_shouldBeOK() {
    final var expectedTicker = "HCTR11";
    final var command = DeleteCompanyCommand.with(expectedTicker);

    doNothing()
            .when(companyGateway).delete(eq(expectedTicker));

    final var output = useCase.execute(command);

    Assertions.assertNull(output);

    Mockito.verify(companyGateway, times(1)).delete(eq(command.ticker()));
  }

  @Test
  public void givenAValidId_whenGatewayThrowsException_shouldReturnException() {
    final var company = Company.create(NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE);
    final var command = DeleteCompanyCommand.with(company.getTicker().getValue());
    String expectedErrorMessage = "[COMPANY]: ".concat(GATEWAY_DELETE_STR);

    doThrow(new IllegalStateException(GATEWAY_DELETE_STR))
            .when(companyGateway).delete(eq(command.ticker()));

    final var output = useCase.execute(command);

    Assertions.assertNotNull(output);
    Assertions.assertTrue(output.notification().hasErrors());
    Assertions.assertEquals(1, output.notification().getErrors().size());
    Assertions.assertEquals(expectedErrorMessage, output.notification().messages(""));


    Mockito.verify(companyGateway, times(1)).delete(eq(command.ticker()));
  }

}
