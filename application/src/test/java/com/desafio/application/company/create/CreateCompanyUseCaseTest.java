package com.desafio.application.company.create;

import com.desafio.application.UseCaseTest;
import com.desafio.domain.company.Company;
import com.desafio.domain.company.CompanyGateway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.List;
import java.util.Objects;

import static com.desafio.utils.Constants.ErrorMessages.GATEWAY_INSERT_STR;
import static com.desafio.utils.Constants.ErrorMessages.STRING_SHOULD_NOT_BE_NULL;
import static com.desafio.utils.Constants.*;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class CreateCompanyUseCaseTest extends UseCaseTest {
  @InjectMocks
  private CreateCompanyUseCase useCase;

  @Mock
  private CompanyGateway companyGateway;

  @Override
  protected List<Object> getMocks() {
    return List.of(companyGateway);
  }

  @Test
  void givenAValidCommand_whenCallsCreateCompany_shouldReturnInstance() {
    final var command =
            CreateCompanyCommand.with(NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true);

    when(companyGateway.create(any()))
            .thenAnswer(returnsFirstArg());

    final var actualOutput = useCase.execute(command);

    Assertions.assertNull(actualOutput.notificationErrors());
    Assertions.assertNotNull(actualOutput.id());

    verify(companyGateway, times(1)).create(argThat(company ->
            Objects.equals(NAME_SAMPLE, company.getName())
                    && Objects.equals(TICKER_SAMPLE, company.getTicker().getValue())
                    && Objects.equals(PRICE_SAMPLE, company.getPrice())
                    && Objects.equals(true, company.isActive())
                    && Objects.nonNull(company.getId())
                    && Objects.nonNull(company.getCreatedAt())
                    && Objects.nonNull(company.getUpdatedAt())
                    && Objects.isNull(company.getDeletedAt())
    ));
  }

  @Test
  void givenAInvalidNullName_whenCallsCreateCompany_thenShouldReturnListOfErrors() {
    String expectedErrorMessage = "[COMPANY]: ".concat(String.format(STRING_SHOULD_NOT_BE_NULL, NAME_STR));
    final var command =
            CreateCompanyCommand.with(null, TICKER_SAMPLE, PRICE_SAMPLE, true);


    final var actualOutput = useCase.execute(command);

    Assertions.assertNotNull(actualOutput.notificationErrors());
    Assertions.assertNull(actualOutput.id());
    Assertions.assertEquals(1, actualOutput.notificationErrors().getErrors().size());
    Assertions.assertEquals(expectedErrorMessage, actualOutput.notificationErrors().messages(""));

    verify(companyGateway, times(0)).create(any());
  }

  @Test
  public void givenAValidCommandWithInactiveCategory_whenCallsCreateCategory_shouldReturnInactiveCategoryId() {
    final var command =
            CreateCompanyCommand.with(NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true);

    when(companyGateway.create(any()))
            .thenAnswer(returnsFirstArg());

    final var actualOutput = useCase.execute(command);

    Assertions.assertNotNull(actualOutput);
    Assertions.assertNull(actualOutput.notificationErrors());
    Assertions.assertNotNull(actualOutput.id());

    verify(companyGateway, times(1)).create(any(Company.class));
  }

  @Test
  public void givenAValidCommand_whenGatewayThrowsRandomException_shouldReturnAException() {
    String expectedErrorMessage = "[COMPANY]: ".concat(GATEWAY_INSERT_STR.replace("{}", TICKER_SAMPLE));
    final var command =
            CreateCompanyCommand.with(NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true);

    when(companyGateway.create(any()))
            .thenThrow(new IllegalStateException(GATEWAY_INSERT_STR));

    final var notification = useCase.execute(command);

    Assertions.assertEquals(1, notification.notificationErrors().getErrors().size());
    Assertions.assertEquals(expectedErrorMessage, notification.notificationErrors().messages(""));

    verify(companyGateway, times(1)).create(any(Company.class));
  }

}
