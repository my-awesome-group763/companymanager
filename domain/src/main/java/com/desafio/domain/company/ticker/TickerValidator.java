package com.desafio.domain.company.ticker;

import com.desafio.notification.INotification;

import static com.desafio.utils.Constants.*;
import static com.desafio.utils.Constants.ErrorMessages.*;

public class TickerValidator implements ITickerValidator {

  @Override
  public void validate(Ticker ticker, INotification notification) {

    if (ticker == null) {
      notification.append(String.format(STRING_SHOULD_NOT_BE_NULL, TICKER_STR), CLASS_STR);
      return;
    } else if (ticker.getValue().isEmpty()) {
      notification.append(String.format(STRING_SHOULD_NOT_BE_BLANK, TICKER_STR), CLASS_STR);
    }

    final String ticker_str = ticker.getValue().trim();
    if ((ticker_str.length() < MIN_TICKER_LEN) || (ticker_str.length() > MAX_TICKER_LEN)) {
      notification.append(TICKER_LENGTH_INVALID, CLASS_STR);
    }
  }

}
