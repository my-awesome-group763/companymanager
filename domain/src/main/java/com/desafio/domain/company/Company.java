package com.desafio.domain.company;

import com.desafio.domain.Aggregate;
import com.desafio.domain.company.ticker.Ticker;
import com.desafio.domain.company.ticker.TickerValidator;
import com.desafio.notification.Notification;

import java.math.BigDecimal;
import java.time.Instant;

public class Company extends Aggregate<CompanyId> {

  private String name;
  private Ticker ticker;
  private BigDecimal price;
  private Instant createdAt;
  private Instant updatedAt;
  private Instant deletedAt;
  private boolean isActive;

  public static Company create(String name, Ticker ticker, BigDecimal price) {
    final Instant now = Instant.now();
    return new Company(CompanyId.unique(), name, ticker, price, now, now, null, true);
  }

  public static Company create(String name, String ticker, BigDecimal price) {
    final Instant now = Instant.now();
    return new Company(CompanyId.unique(), name, Ticker.create(ticker), price, now, now, null, true);
  }

  private Company(CompanyId companyId, String name, Ticker ticker, BigDecimal price, Instant createdAt, Instant updatedAt, Instant deletedAt, boolean isActive) {
    super(companyId);
    this.name = name;
    this.ticker = ticker;
    this.price = price;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
    this.isActive = isActive;
    this.setNotification(new Notification());
  }

  public Company update(
          String name,
          Ticker ticker,
          BigDecimal price,
          boolean isActive
  ) {
    if(isActive) {
      this.activate();
    } else {
      this.deactivate();
    }
    this.name = name;
    this.ticker = ticker;
    this.price = price;
    this.updatedAt = Instant.now();
    return this;
  }

  @Override
  public void validate() {
    new CompanyValidator(new TickerValidator()).validate(this);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Ticker getTicker() {
    return ticker;
  }

  public void setTicker(Ticker ticker) {
    this.ticker = ticker;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public Instant getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Instant createdAt) {
    this.createdAt = createdAt;
  }

  public Instant getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Instant updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Instant getDeletedAt() {
    return deletedAt;
  }

  public void setDeletedAt(Instant deletedAt) {
    this.deletedAt = deletedAt;
  }

  public boolean isActive() {
    return isActive;
  }

  public void setActive(boolean active) {
    isActive = active;
  }

  public Company activate() {
    this.deletedAt = null;
    this.isActive = true;
    this.updatedAt = Instant.now();
    return this;
  }

  public Company deactivate() {
    if (getDeletedAt() == null) {
      this.deletedAt = Instant.now();
    }

    this.isActive = false;
    this.updatedAt = Instant.now();
    return this;
  }
}
