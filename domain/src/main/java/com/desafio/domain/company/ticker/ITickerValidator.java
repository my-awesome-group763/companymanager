package com.desafio.domain.company.ticker;

import com.desafio.notification.INotification;

public interface ITickerValidator {
  public abstract void validate(Ticker ticker, INotification notification);
}
