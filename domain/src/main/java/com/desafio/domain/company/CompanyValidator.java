package com.desafio.domain.company;

import com.desafio.domain.Validator;
import com.desafio.domain.company.ticker.ITickerValidator;

import java.math.BigDecimal;

import static com.desafio.utils.Constants.*;
import static com.desafio.utils.Constants.ErrorMessages.*;

public class CompanyValidator extends Validator<Company> {

  private ITickerValidator iTickerValidator;

  public CompanyValidator(ITickerValidator iTickerValidator) {
    this.iTickerValidator = iTickerValidator;
  }

  @Override
  public void validate(Company company) {
    checkName(company);
    checkPrice(company);
    this.iTickerValidator.validate(company.getTicker(), company.getNotification());
  }

  private void checkPrice(Company company) {
    if (company.getPrice() == null){
      company.getNotification().append(String.format(STRING_SHOULD_NOT_BE_NULL, PRICE_STR), CLASS_STR);
      return;
    }
    else if (company.getPrice().compareTo(BigDecimal.valueOf(0)) < 0)
      company.getNotification().append(String.format(PRICE_INVALID, PRICE_STR), CLASS_STR);
  }

  private void checkName(Company company) {
    if (company.getName() == null) {
      company.getNotification().append(String.format(STRING_SHOULD_NOT_BE_NULL, NAME_STR), CLASS_STR);
      return;
    } else if (company.getName().isEmpty()) {
      company.getNotification().append(String.format(STRING_SHOULD_NOT_BE_BLANK, NAME_STR), CLASS_STR);
    }

    final String name = company.getName().trim();
    if ((name.length() < MIN_NAME_LEN) || (name.length() > MAX_NAME_LEN)) {
      company.getNotification().append(NAME_LENGTH_INVALID, CLASS_STR);
    }
  }
}
