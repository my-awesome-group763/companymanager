package com.desafio.domain.company.ticker;

import com.desafio.domain.ValueObject;
import com.desafio.domain.user.cpf.Cpf;

import java.util.Objects;

public class Ticker extends ValueObject {
  private final String value;

  private Ticker(String value) {
    this.value = value;
  }

  public static Ticker create(String value) {
    return new Ticker(value);
  }

  public String getValue() {
    return value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Ticker ticker = (Ticker) o;
    return getValue().equals(ticker.getValue());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getValue());
  }
}
