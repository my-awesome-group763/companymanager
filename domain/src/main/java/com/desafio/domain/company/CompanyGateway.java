package com.desafio.domain.company;

import java.util.List;
import java.util.Optional;

public interface CompanyGateway {
  Company create(Company company);
  Company update(Company company);
  List<Company> findAll();
  Optional<Company> findByTicker(String ticker);
  void delete(String ticker);
}
