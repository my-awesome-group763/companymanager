package com.desafio.domain.user.cpf;

import com.desafio.domain.ValueObject;

import java.util.Objects;

public class Cpf extends ValueObject {

  private final String value;

  private Cpf(String value) {
    this.value = value;
  }

  public static Cpf create(String value) {
    return new Cpf(value);
  }

  public static Cpf format(String value) {
    return new Cpf(value.trim().replaceAll("[^\\d ]", ""));
  }

  public String getValue() {
    return value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Cpf cpf = (Cpf) o;
    return getValue().equals(cpf.getValue());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getValue());
  }
}
