package com.desafio.domain.user;

import java.util.Optional;

public interface UserGateway {

  User create(User user);
  Optional<User> findByCpf(String cpf);
  void delete(String cpf);

}
