package com.desafio.domain.user;

import com.desafio.domain.Aggregate;
import com.desafio.domain.user.cpf.Cpf;
import com.desafio.domain.user.cpf.CpfValidator;
import com.desafio.domain.user.handler.afterValidation.IAfterValidation;
import com.desafio.domain.user.handler.afterValidation.formatCpf.FormatCpf;
import com.desafio.domain.user.handler.afterValidation.removePunctuation.RemovePunctuationFromName;
import com.desafio.notification.Notification;

import java.util.List;

public class User extends Aggregate<UserId> {
  private String name;
  private Cpf cpf;
  private boolean isAdm;

  public static User create(String name, String cpf) {
    return new User(name, Cpf.create(cpf), false);
  }

  public static User createAdm(String name, String cpf) {
    return new User(name, Cpf.create(cpf), true);
  }

  private User(String name, Cpf cpf, boolean isAdm) {
    super(UserId.unique());
    this.name = name;
    this.cpf = cpf;
    this.isAdm = isAdm;
    this.setNotification(new Notification());
  }

  @Override
  public void validate() {
    List<IAfterValidation<User>> validationHandlers = List.of(new RemovePunctuationFromName(), new FormatCpf());
    new UserValidator(new CpfValidator(), validationHandlers).validate(this);
  }

  public void changeName(String name) {
    this.name = name;
  }

  public void formatCpf() {
    this.cpf = Cpf.format(this.cpf.getValue());
  }

  public UserId getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public Cpf getCpf() {
    return cpf;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setCpf(Cpf cpf) {
    this.cpf = cpf;
  }

  public boolean isAdm() {
    return isAdm;
  }

  public void setAdm(boolean adm) {
    isAdm = adm;
  }
}
