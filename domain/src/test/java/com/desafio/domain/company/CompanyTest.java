package com.desafio.domain.company;

import com.desafio.domain.company.ticker.Ticker;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.desafio.utils.Constants.ErrorMessages.*;
import static com.desafio.utils.Constants.*;

public class CompanyTest {

  @Test
  void givenValidParams_whenCallCreateCompany_thenShouldCreateCompany() {
    final Ticker ticker = Ticker.create(TICKER_SAMPLE);
    final Company createdCompany = Company.create(NAME_SAMPLE, ticker, PRICE_SAMPLE);
    createdCompany.validate();

    Assertions.assertNotNull(createdCompany);
    Assertions.assertNotNull(createdCompany.getId());
    Assertions.assertNotNull(createdCompany.getCreatedAt());
    Assertions.assertNotNull(createdCompany.getUpdatedAt());
    Assertions.assertNull(createdCompany.getDeletedAt());
    Assertions.assertTrue(createdCompany.isActive());
    Assertions.assertEquals(NAME_SAMPLE, createdCompany.getName());
    Assertions.assertEquals(TICKER_SAMPLE, createdCompany.getTicker().getValue());
    Assertions.assertEquals(PRICE_SAMPLE, createdCompany.getPrice());
  }

  @Test
  void givenInvalidNullName_whenCallCreateCompany_thenReturnListOfErrors() {
    final Integer expected_error_count = 1;
    final String expected_error_message = String.format(STRING_SHOULD_NOT_BE_NULL, NAME_STR);

    final Ticker ticker = Ticker.create(TICKER_SAMPLE);
    final Company company = Company.create(null, ticker, PRICE_SAMPLE);
    company.validate();

    Assertions.assertEquals(expected_error_count, company.getNotification().getErrors().size());
    Assertions.assertEquals(expected_error_message, company.getNotification().getErrors().get(0).message());
  }

  @Test
  void givenInvalidBlankName_whenCallCreateCompany_thenReturnListOfErrors() {
    final Integer expected_error_count = 2;

    final Ticker ticker = Ticker.create(TICKER_SAMPLE);
    final Company company = Company.create("", ticker, PRICE_SAMPLE);
    company.validate();

    Assertions.assertEquals(expected_error_count, company.getNotification().getErrors().size());
  }

  @Test
  void givenInvalidNameLengthLesserThan3_whenCallCreateCompany_thenReturnListOfErrors() {
    final String expected_name = "Fi ";

    final String expected_error_message = NAME_LENGTH_INVALID;
    final Integer expected_error_count = 1;

    final Ticker ticker = Ticker.create(TICKER_SAMPLE);
    final Company company = Company.create(expected_name, ticker, PRICE_SAMPLE);
    company.validate();

    Assertions.assertEquals(expected_error_count, company.getNotification().getErrors().size());
    Assertions.assertEquals(expected_error_message, company.getNotification().getErrors().get(0).message());
  }

  @Test
  void givenInvalidNameLengthGreaterThan30_whenCallCreateCompany_thenReturnListOfErrors() {
    final Integer expected_error_count = 1;

    final Ticker ticker = Ticker.create(TICKER_SAMPLE);
    final Company company = Company.create(LERO_LERO, ticker, PRICE_SAMPLE);
    company.validate();

    Assertions.assertEquals(expected_error_count, company.getNotification().getErrors().size());
    Assertions.assertEquals(NAME_LENGTH_INVALID, company.getNotification().getErrors().get(0).message());
  }

  @Test
  void givenInvalidNullTicker_whenCallCreateCompany_thenReturnListOfErrors() {
    final Integer expected_error_count = 1;
    final String expected_error_message = String.format(STRING_SHOULD_NOT_BE_NULL, TICKER_STR);

    Company company = Company.create(NAME_SAMPLE, (Ticker) null, PRICE_SAMPLE);
    company.validate();

    Assertions.assertEquals(expected_error_count, company.getNotification().getErrors().size());
    Assertions.assertEquals(expected_error_message, company.getNotification().getErrors().get(0).message());
  }

  @Test
  void givenInvalidBlankTicker_whenCallCreateCompany_thenReturnListOfErrors() {
    final Integer expected_error_count = 2;

    Company company = Company.create(NAME_SAMPLE, Ticker.create(""), PRICE_SAMPLE);
    company.validate();

    Assertions.assertEquals(expected_error_count, company.getNotification().getErrors().size());
  }

  @Test
  void givenInvalidTickerLengthLesserThan5_whenCallCreateCompany_thenReturnListOfErrors() {
    final Ticker expected_ticker = Ticker.create("Fi ");

    final String expected_error_message = TICKER_LENGTH_INVALID;
    final Integer expected_error_count = 1;

    Company company = Company.create(NAME_SAMPLE, expected_ticker, PRICE_SAMPLE);
    company.validate();

    Assertions.assertEquals(expected_error_count, company.getNotification().getErrors().size());
    Assertions.assertEquals(expected_error_message, company.getNotification().getErrors().get(0).message());
  }

  @Test
  void givenInvalidTickerLengthGreaterThan7_whenCallCreateCompany_thenReturnListOfErrors() {
    final String expected_error_message = TICKER_LENGTH_INVALID;
    final Integer expected_error_count = 1;

    Company company = Company.create(NAME_SAMPLE, Ticker.create(LERO_LERO), PRICE_SAMPLE);
    company.validate();

    Assertions.assertEquals(expected_error_count, company.getNotification().getErrors().size());
    Assertions.assertEquals(expected_error_message, company.getNotification().getErrors().get(0).message());
  }


  @Test
  void givenInvalidBlankNameAndTicker_whenCallCreateCompany_thenReturnListOfErrors() {
    final Integer expected_error_count = 4;

    Company company = Company.create("", Ticker.create(""), PRICE_SAMPLE);
    company.validate();

    Assertions.assertEquals(expected_error_count, company.getNotification().getErrors().size());
  }

  @Test
  void givenInvalidNullPrice_whenCallCreateCompany_thenReturnListOfErrors() {
    final Integer expected_error_count = 1;
    final String expected_error_message = String.format(STRING_SHOULD_NOT_BE_NULL, PRICE_STR);

    Company company = Company.create(NAME_SAMPLE, Ticker.create(TICKER_SAMPLE), null);
    company.validate();

    Assertions.assertEquals(expected_error_count, company.getNotification().getErrors().size());
    Assertions.assertEquals(expected_error_message, company.getNotification().getErrors().get(0).message());
  }

  @Test
  void givenInvalidNegativePrice_whenCallCreateCompany_thenReturnListOfErrors() {
    final Integer expected_error_count = 1;

    Company company = Company.create(NAME_SAMPLE, Ticker.create(TICKER_SAMPLE), BigDecimal.valueOf(-10));
    company.validate();

    Assertions.assertEquals(expected_error_count, company.getNotification().getErrors().size());
    Assertions.assertEquals(PRICE_INVALID, company.getNotification().getErrors().get(0).message());
  }

  @Test
  void givenInvalidBlankNameAndTickerAndNullPrice_whenCallCreateCompany_thenReturnListOfErrors() {
    final Integer expected_error_count = 5;

    Company company = Company.create("", Ticker.create(""), null);
    company.validate();

    Assertions.assertEquals(expected_error_count, company.getNotification().getErrors().size());
  }

  @Test
  public void givenAValidActiveCompany_whenCallDeactivate_thenReturnCompanyInactivated() {
    Company company = Company.create(NAME_SAMPLE, Ticker.create(TICKER_SAMPLE), PRICE_SAMPLE);
    company.validate();

    Assertions.assertTrue(company.isActive());
    Assertions.assertNull(company.getDeletedAt());

    final var actualCompany = company.deactivate();
    company.validate();

    Assertions.assertNotNull(actualCompany);
    Assertions.assertNotNull(actualCompany.getId());
    Assertions.assertNotNull(actualCompany.getCreatedAt());
    Assertions.assertNotNull(actualCompany.getUpdatedAt());
    Assertions.assertNotNull(actualCompany.getDeletedAt());
    Assertions.assertFalse(actualCompany.isActive());
    Assertions.assertEquals(NAME_SAMPLE, actualCompany.getName());
    Assertions.assertEquals(TICKER_SAMPLE, actualCompany.getTicker().getValue());
    Assertions.assertEquals(PRICE_SAMPLE, actualCompany.getPrice());
  }

  @Test
  public void givenAValidInactiveCompany_whenCallActivate_thenReturnCompanyActivated() {
    Company company = Company.create(NAME_SAMPLE, Ticker.create(TICKER_SAMPLE), PRICE_SAMPLE);
    company = company.deactivate();
    company.validate();

    Assertions.assertFalse(company.isActive());
    Assertions.assertNotNull(company.getDeletedAt());

    final var actualCompany = company.activate();
    company.validate();

    Assertions.assertNotNull(actualCompany);
    Assertions.assertNotNull(actualCompany.getId());
    Assertions.assertNotNull(actualCompany.getCreatedAt());
    Assertions.assertNotNull(actualCompany.getUpdatedAt());
    Assertions.assertNull(actualCompany.getDeletedAt());
    Assertions.assertTrue(actualCompany.isActive());
    Assertions.assertEquals(NAME_SAMPLE, actualCompany.getName());
    Assertions.assertEquals(TICKER_SAMPLE, actualCompany.getTicker().getValue());
    Assertions.assertEquals(PRICE_SAMPLE, actualCompany.getPrice());
  }

  @Test
  public void givenAValidCompany_whenCallUpdate_thenReturnCompanyUpdated() {
    Ticker expectedTickerAfterUpdate = Ticker.create("BIDI4");
    Company company = Company.create(NAME_SAMPLE, Ticker.create(TICKER_SAMPLE), PRICE_SAMPLE);
    company.validate();

    Assertions.assertFalse(company.getNotification().hasErrors());

    final var createdAt = company.getCreatedAt();
    final var updatedAt = company.getUpdatedAt();

    final var actualCompnay = company.update(NAME_SAMPLE, expectedTickerAfterUpdate, PRICE_SAMPLE, true);
    actualCompnay.validate();

    Assertions.assertEquals(company.getId(), actualCompnay.getId());
    Assertions.assertEquals(NAME_SAMPLE, actualCompnay.getName());
    Assertions.assertEquals(expectedTickerAfterUpdate, actualCompnay.getTicker());
    Assertions.assertTrue(actualCompnay.isActive());
    Assertions.assertEquals(createdAt, actualCompnay.getCreatedAt());
    Assertions.assertTrue(actualCompnay.getUpdatedAt().isAfter(updatedAt));
    Assertions.assertNull(actualCompnay.getDeletedAt());
  }

  @Test
  public void givenAValidCompany_whenCallUpdateToInactive_thenReturnCompanyUpdated() {
    Company company = Company.create(NAME_SAMPLE, Ticker.create(TICKER_SAMPLE), PRICE_SAMPLE);
    company.validate();

    Assertions.assertTrue(company.isActive());
    Assertions.assertNull(company.getDeletedAt());

    final var createdAt = company.getCreatedAt();
    final var updatedAt = company.getUpdatedAt();

    final var actualCompany = company.update(NAME_SAMPLE, Ticker.create(TICKER_SAMPLE), BigDecimal.valueOf(3), false);
    actualCompany.validate();

    Assertions.assertEquals(company.getId(), actualCompany.getId());
    Assertions.assertEquals(NAME_SAMPLE, actualCompany.getName());
    Assertions.assertEquals(TICKER_SAMPLE, actualCompany.getTicker().getValue());
    Assertions.assertFalse(company.isActive());
    Assertions.assertEquals(createdAt, actualCompany.getCreatedAt());
    Assertions.assertTrue(actualCompany.getUpdatedAt().isAfter(updatedAt));
    Assertions.assertNotNull(company.getDeletedAt());
  }

}
