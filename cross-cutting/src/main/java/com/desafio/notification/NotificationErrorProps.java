package com.desafio.notification;

public record NotificationErrorProps(String message, String context) { }
